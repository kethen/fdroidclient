# F-Droid multi-platform download library

Note that advanced security and privacy features are only available for Android:

   * Rejection of TLS 1.1 and older as well as rejection of weak ciphers
   * No DNS requests when using Tor as a proxy
   * short TLS session timeout to prevent tracking and key re-use

Other platforms besides Android have not been tested and might need additional work.
